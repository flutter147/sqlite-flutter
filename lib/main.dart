import 'cat.dart';
import 'cat_dao.dart';
import 'dog.dart';
import 'dog_dao.dart';

void main() async {
  var fido = Dog(id: 0, name: 'Fido', age: 35);
  var dido = Dog(id: 1, name: 'Dido', age: 20);
  await DogDao.insertDog(fido);
  await DogDao.insertDog(dido);

  print(await DogDao.dogs());

  fido = Dog(id: fido.id, name: fido.name, age: fido.age + 7);

  await DogDao.updateDog(fido);
  print(await DogDao.dogs());

  await DogDao.deleteDog(0);
  print(await DogDao.dogs());

  var fado = Cat(id: 0, name: 'Fado', age: 19);
  var dado = Cat(id: 1, name: 'Dado', age: 15);
  await CatDao.insertCat(fado);
  await CatDao.insertCat(dado);

  print(await CatDao.cats());

  fado = Cat(id: fido.id, name: fido.name, age: fido.age + 10);

  await CatDao.updateCat(fado);
  print(await CatDao.cats());

  await CatDao.deleteCat(0);
  print(await CatDao.cats());
}
