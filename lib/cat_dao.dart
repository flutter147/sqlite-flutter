import 'package:sqflite/sqflite.dart';

import 'database_provider.dart';
import 'cat.dart';

class CatDao {
  static Future<void> insertCat(Cat cat) async {
    final db = await DatabaseProvider.database;
    await db.insert('dogs', cat.toMap(),
        conflictAlgorithm: ConflictAlgorithm.replace);
  }

  static Future<List<Cat>> cats() async {
    final db = await DatabaseProvider.database;
    return Cat.toList(await db.query('dogs'));
  }

  static Future<void> updateCat(Cat cat) async {
    final db = await DatabaseProvider.database;

    await db.update('dogs', cat.toMap(), where: 'id = ?', whereArgs: [cat.id]);
  }

  static Future<void> deleteCat(int id) async {
    final db = await DatabaseProvider.database;
    await db.delete('dogs', where: 'id = ?', whereArgs: [id]);
  }
}
